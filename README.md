# OpenCity

Originally developed at http://www.opencity.info/ and https://sourceforge.net/projects/opencity/ by [neoneurone](https://sourceforge.net/u/neoneurone/) and published under the GPL-2.0 license.